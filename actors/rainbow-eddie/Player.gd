extends KinematicBody2D

export (int) var run_speed = 250
export (int) var jump_speed = -400
export (int) var gravity = 900

var velocity = Vector2()
var jumping = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func get_input():
	# Detect up/down/left/right keystate and only move when pressed.
	velocity.x = 0
	var down = Input.is_action_pressed("move_down")
	var right = Input.is_action_pressed("move_right")
	var left = Input.is_action_pressed("move_left")
	var jump = !down and Input.is_action_pressed("move_up")
	
	var anim = $Visual/AnimatedSprite
	
	if jump and is_on_floor():
		jumping = true
		velocity.y = jump_speed
		$Visual/Particles.emitting = true;
		
	if right:
		velocity.x += run_speed
		
	if left:
		velocity.x -= run_speed
		
	if velocity.x != 0:
		if velocity.x < 0:
			$Visual.scale.x = abs($Visual.scale.x)
		else:
			$Visual.scale.x = -abs($Visual.scale.x)
	
	if left or right or jump:
		if jump or jumping:
			anim.animation = "jumping"
			if is_on_floor():
				anim.frame = 0
		else:
			anim.animation = "running"
		
		if !anim.is_playing() and velocity.length_squared() >= 0.01:
			anim.frame = 0
			anim.play()
	else:
		if anim.is_playing() and is_on_floor():
			anim.stop()
			anim.animation = "running"
			anim.frame = 0
	
	#$Visual/Rainbow.visible = anim.animation == "jumping" and abs(velocity.x) >= 1
	$Shield.visible = down and is_on_floor() and abs(velocity.x) < 1

func _physics_process(delta):
	get_input()
	velocity.y += gravity * delta
	if jumping and is_on_floor():
		jumping = false
	
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	if abs(velocity.x) < 0.1 or is_on_floor():
		 $Visual/Particles.emitting = false
