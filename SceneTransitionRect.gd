extends ColorRect

# Tutorial: https://www.gdquest.com/tutorial/godot/2d/scene-transition-rect/

onready var _anim_player := $Fade

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = true
	_anim_player.play_backwards("Fade")

# Plays the Fade animation and waits until it finishes
func do_transition():
	_anim_player.play("Fade")
	
	# Resume execution when animation is done playing.
	yield(_anim_player, "animation_finished")
