extends Node2D

signal on_level_start(level)

func reset_player_position():
	$Player.position = $StartingPosition.position

# Replaces this node with the next level and moves the player's node to the next level
func next_level():
	# Get the number of the next next level and construct the resource path
	var num = int(name) + 1
	var nextLevelPath = "res://levels/" + str(num) + "/Level_" + str(num) + ".tscn"
	
	print("Changing to " + nextLevelPath)
	
	# Load the scene of the next level, return if not found 
	var nextLevelScene = load(nextLevelPath)
	if nextLevelScene == null:
		print("Level not found: " + nextLevelPath)
		return
	
	# Create an instance of the next level
	var nextLevel = nextLevelScene.instance()
	print("Loaded " + nextLevel.name)
	
	# Move the player from the current level to the next level
	var player = $Player
	remove_child(player)
	nextLevel.add_child(player)
	nextLevel.reset_player_position()
	print("Moved " + player.name + " to the next level")
	
	# Level-transition effect
	$GUI/SceneTransitionRect.do_transition()
	
	# Schedule replacing the curent level with the next level
	queue_free()
	get_tree().root.call_deferred("add_child", nextLevel)

func _on_Level_tree_entered():
	emit_signal("on_level_start", self)

func _on_ExitArea_body_entered(_body):
	next_level()

func _input(_event):
	if Input.is_action_just_released("ui_cancel"):
		print("Exit")
		get_tree().quit()
