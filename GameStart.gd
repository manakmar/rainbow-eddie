extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	# Load the first level and instantiate a player
	var level = load("res://levels/1/Level_1.tscn").instance()
	var player = load("res://actors/rainbow-eddie/Player.tscn").instance()
	
	# Prepare the first level - add a player on the starting position
	level.add_child(player)
	level.reset_player_position()
	
	# Replace the GameStart node with the first level 
	queue_free()
	get_tree().root.call_deferred("add_child", level)
